# README #

### What is this repository for? ###

This repository is created for the assignment of TCP/IP subject, with a C program to retrieve local IP address on Unix-based OS.

### How do I get set up? ###

* Source Tree
1. Click on "Clone" and copy paste the URL to create a local repository on the computer.

* Through terminal (Unix)
1. Enter "$ sudo apt-get install git" to install git on the computer.
2. Clone using the command line available on the left-sided bar. 

### Contribution guidelines ###

1. Amend/Create a new file in the local repository cloned from BitBucket.
2. Tick on "Unstaged Files" and click on "Commit"
3. Enter commit messages before commiting.
4. Fetch from all remote and pull from other admins' work for code synchronization.
5. Push the selected committed files to the Internet on BitBucket.

### Who do I talk to? ###

* Repo owner or admin
Programmer: Wendy Goh Wen Yit (1122701837)
Debugger: Chua Hui Khen (1122701414)